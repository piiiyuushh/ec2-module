variable "ami" {
  description = "Ami id of instance"
}
variable "instance_type" {
  description = "Choosing instance type"
}
variable "subnet_id" {
  description = "Giving subnet id"
}
variable "key_name" {
  description = "Giving public key"
}
variable "tenancy" {
  description = ""
}
variable "availability_zone" {
  description = "Giving availability zone"
}
variable "instance_tag" {
  description = ""
}
variable "vpcsg" {
  description = ""
}
variable "ec2inbound_ports" {
  description = ""
}
variable "ingr_protocol" {
  description = ""
}
variable "ingr_cidr_block" {
  description = ""
}
#-----------------asg-launch-template--------------------
variable "name_prefix" {
  description = "Name which you define "
}
variable "image_id" {
  description = "Instance image "
}
variable "instance_type2" {
  description = "Type of instance"
}
variable "key_name2" {
  description = "key for accessing ec2 "
}
#--------------asg-group-------------------
# variable "availability_zones2" {
#   default = ""
# description = "the availability zone which you defined "
# }
variable "name2" {
  description = "Name which you define "
}
variable "max_size" {
  description = "maxximum size of instances"
}
variable "min_size" {
  description = "minnimum required instances"
}
variable "desired_capacity" {
  description = "desired instances you required "
}
variable "health_check_grace_period" {
  description = ""
}
variable "health_check_type" {
  description = "The Autoscaling will happen based on health of AWS EC2 instance defined in AWS CLoudwatch Alarm "
}
#-------------aasg-policy-----------------
variable "name3" {
  description = "name of asg policy"
}
variable "scaling_adjustment" {
  description = ""
}
variable "adjustment_type" {
  description = ""
}
variable "cooldown" {
  description = ""
}
#-----------------Cloudwatch------------------------------
variable "alarm_name" {
  description = ""
}
variable "comparison_operator" {
  description = ""
}
variable "evaluation_periods" {
  description = ""
}
variable "metric_name" {
  description = ""
}
variable "namespace" {
  description = ""
}
variable "period" {
  description = ""
}
variable "statistic" {
  description = ""
}
variable "threshold" {
  description = ""
}
#--------------------albsg---------------------------------#
variable "vpcalb" {
  description = ""
}
variable "alb_inbound_ports" {
  description = ""
}
variable "alb_ingr_protocol" {
  description = ""
}
variable "alb_ingr_cidr_block" {
  description = ""
}
#-----------albsubnets
variable "alb_subnets" {
  description = ""
}
