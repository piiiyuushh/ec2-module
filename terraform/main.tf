module "ec2" {
  source            = "./modules/ec2"
  ami               = var.ami
  instance_type     = var.instance_type
  subnet_id         = var.subnet_id
  key_name          = var.key_name
  tenancy           = var.tenancy
  availability_zone = var.availability_zone
  instance_tag      = var.instance_tag
  name_prefix       = var.name_prefix
  image_id          = var.image_id
  instance_type2    = var.instance_type2
  key_name2         = var.key_name2
  #--------------------------ec2sg---------------------------#
  vpcsg            = var.vpcsg
  ec2inbound_ports = var.ec2inbound_ports
  ingr_protocol    = var.ingr_protocol
  ingr_cidr_block  = var.ingr_cidr_block
  #availability_zones2       = var.availability_zones2
  name2                     = var.name2
  max_size                  = var.max_size
  min_size                  = var.min_size
  desired_capacity          = var.desired_capacity
  health_check_grace_period = var.health_check_grace_period
  health_check_type         = var.health_check_type
  name3                     = var.name3
  scaling_adjustment        = var.scaling_adjustment
  adjustment_type           = var.adjustment_type
  cooldown                  = var.cooldown
  alarm_name                = var.alarm_name
  comparison_operator       = var.comparison_operator
  evaluation_periods        = var.evaluation_periods
  metric_name               = var.metric_name
  namespace                 = var.namespace
  period                    = var.period
  statistic                 = var.statistic
  threshold                 = var.threshold
  #----------------------------albsg---------------------------#
  vpcalb              = var.vpcalb
  alb_inbound_ports   = var.alb_inbound_ports
  alb_ingr_protocol   = var.alb_ingr_protocol
  alb_ingr_cidr_block = var.alb_ingr_cidr_block
  #--------albsaubnets
  alb_subnets = var.alb_subnets
}
 