resource "aws_security_group" "ec2sg" {
  name = "EC2-SG"
  vpc_id = var.vpcsg
  dynamic "ingress" {
    for_each = var.ec2inbound_ports
    content{
      from_port = ingress.value
      to_port = ingress.value
      protocol = var.ingr_protocol
      cidr_blocks = var.ingr_cidr_block
    }
  }
      egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
      }
}
resource "aws_instance" "Instance" {
  ami           = var.ami
  instance_type = var.instance_type
  subnet_id     = var.subnet_id
  key_name      = var.key_name
  tenancy       = var.tenancy 
  availability_zone = var.availability_zone
  vpc_security_group_ids = [aws_security_group.ec2sg.id] 
  root_block_device {
    delete_on_termination = "false"
  }

  tags = {
    Name = var.instance_tag
  }
}

resource "aws_launch_template" "autoscale" {
  name  = var.name_prefix
  image_id      = var.image_id
  instance_type = var.instance_type2
  key_name = var.key_name2
  vpc_security_group_ids = [aws_security_group.ec2sg.id]
}

# Creating the autoscaling group within us-east-1a availability zone
resource "aws_autoscaling_group" "mygroup" {
  availability_zones        = ["ap-northeast-1d"]
  name                      = var.name2
  max_size                  = var.max_size
  min_size                  = var.min_size
  desired_capacity          = var.desired_capacity 
  health_check_grace_period = var.health_check_grace_period
  health_check_type         = var.health_check_type
  force_delete              = "true"
  termination_policies      = ["OldestInstance"]
  target_group_arns = [aws_lb_target_group.test.arn]

  launch_template {
    id      = aws_launch_template.autoscale.id
  }
} 
  

# Creating the autoscaling policy of the autoscaling group
resource "aws_autoscaling_policy" "mygroup_policy" {
  name                   = var.name3
  scaling_adjustment     = var.scaling_adjustment
  adjustment_type        = var.adjustment_type
  cooldown               = var.cooldown
  autoscaling_group_name = aws_autoscaling_group.mygroup.name
}

resource "aws_cloudwatch_metric_alarm" "web_cpu_alarm_up" {
  alarm_name = var.alarm_name
  comparison_operator = var.comparison_operator
  evaluation_periods = var.evaluation_periods
  metric_name = var.metric_name
  namespace = var.namespace
  period = var.period
  statistic = var.statistic
  threshold = var.threshold
   dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.mygroup.name
  }

  alarm_description = "This metric monitors ec2 cpu utilization"
  alarm_actions     = [aws_autoscaling_policy.mygroup_policy.arn]
}

resource "aws_security_group" "albsg" {
  name = "Alb-SG"
  vpc_id = var.vpcalb
  dynamic "ingress" {
    for_each = var.alb_inbound_ports
    content{
      from_port = ingress.value
      to_port = ingress.value
      protocol = var.alb_ingr_protocol
      cidr_blocks = var.alb_ingr_cidr_block
  
    }
  }
      egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
      }
  }

resource "aws_lb_target_group" "test" {
  name     = "tf-example-lb-tg"
  port     = 8080
  protocol = "HTTP"
  vpc_id   = "vpc-07e0b7f43ae67e444"
  }

resource "aws_lb_target_group_attachment" "test" {
  target_group_arn = aws_lb_target_group.test.arn
  target_id        = aws_instance.Instance.id
  port             = 80
}

resource "aws_lb" "EC2-lb" {
  name               = "test-lb-tf"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.albsg.id]
  subnets            = var.alb_subnets
}

resource "aws_lb_listener" "front_end" {
  load_balancer_arn = aws_lb.EC2-lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.test.arn
  }
}